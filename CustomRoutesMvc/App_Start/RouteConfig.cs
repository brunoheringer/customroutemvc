﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CustomRoutesMvc
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                        name: "RouteEvent",
                        url: "{Evento}",
                        defaults: new { controller = "Evento", action = "Details" },
                        constraints: new { Evento = new EventoConstraint() }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }

    public class EventoConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            return !Assembly.GetAssembly(typeof(MvcApplication))
                .GetTypes().Where(type => typeof(Controller).IsAssignableFrom(type))
                .Any(c => c.Name.Replace("Controller", "").ToLower() == values[parameterName].ToString().ToLower());
        }
    }
}
