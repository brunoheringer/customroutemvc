# README #

This Project is to reference of use of Custom Routes in MVC. 
In this use case, the necessity  is call a URL with a single paramter hiding the "controller" and "action". 


### What is this repository for? ###
Use to Reference of use this case:
1) how-to-create-a-mvc-route-only-with-paramter-in-url
2) MVC Rotas Customizadas | Criar uma Rota somente com um parâmetro na URL


### How do I get set up? ###

* Summary of set up
Use the native ASP.NET MVC

### Contribution guidelines ###
My questions on StackOverFlow:
[EN](https://stackoverflow.com/questions/45764679/how-to-create-a-mvc-route-only-with-paramter-in-url)
[PT](https://pt.stackoverflow.com/questions/230325/mvc-rotas-customizadas-criar-uma-rota-somente-com-um-par%C3%A2metro-na-url)

### Who do I talk to? ###
* Repo owner or admin
* [Bruno Heringer](brunoheringer@gmail.com)